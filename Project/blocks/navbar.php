<nav class="navbar navbar-expand bg-primary">
  <div class="container-fluid">
    <a class="navbar-brand" href="index.php">BookStore</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="#">Home</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            About Us
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="blocks/termspage.php">read terms</a></li>
          </ul>
        </li>
        <li>
            <form class="d-flex" role="search" >
        <input style="margin-left: 100px; padding-right: 200px;" class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success" type="submit">Search</button>
      </form>
        </li>
      </ul>
      <a style="margin-right: 10px;" class="btn btn-success" href="blocks/login.php" role="button">login</a> 
      <a class="btn btn-secondary" href="blocks/register.php" role="button">Register</a>
      
    </div>
  </div>
</nav>