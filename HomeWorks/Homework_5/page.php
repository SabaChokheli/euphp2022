<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<?php

    if(isset($_POST['submit'])){
        $first_name = $_POST['firstname'];
        $last_name = $_POST['lastname'];
        $id_number = $_POST['numbers'];
        $address = $_POST['address'];
        $error = '';
        $error_name = $error_lastn = $id_number = $address ='';
        
        if(empty($first_name)){
            $error_name = 'Is Empty';
        }
        elseif(strlen($first_name) <2 ){
        $error_name = 'Min 2 simbols';
        }elseif(strlen($first_name)>20){
        $error_name = 'max 20 simbols';
        }


        if(empty($last_name)){
            $error_lastn = 'Is Empty';
        }
        if(strlen($last_name) <3 ){
        $error_lastn = 'Min 2 simbols';
        }elseif(strlen($last_name)>50){
        $error_lastn = 'max 20 simbols';
        }
        
        if(empty($address)){
            $error_address = 'Is Empty';
        }elseif(strlen($address)>=70){
            $error_address = 'max 70 simbols';
        }

        if(empty($id_number)){
            $error_idnumber = 'is empty';
        }
        elseif(strlen($id_number)!=11 && !is_numeric($id_number)){
            $error_idnumber = 'is incorrect';
        }
    }  
    
?>
    
    <form method="POST" action="action.php">
        <input type="text" name="firstname" placeholder="Enter FirstName"><span><?=$error_name?></span>
        <input type="text" name="lastname" placeholder="Enter LastName"><span><?=$error_lastn?></span>
        <input type="text" name="date" placeholder="Birthday">
        <input type="text" name="numbers" placeholder="Enter ID numbers"><span><?=$error_idnumber?></span>
        <input type="text" name="address" placeholder="Enter Address"><span><?=$error_address?></span>
        <input type="text" name="date_today">
        <input type="number" name="phone_number" placeholder="Enter Phone Number"><br><br>
        <textarea name="textinfo" placeholder="Info"></textarea><br><br>
        <button name="submit">Submit</button>
    </form>
</body>
</html>