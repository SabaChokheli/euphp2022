<h1>SELECT</h1>
<hr>
<table class="data-table">
    <tbody>
    <?php
    $select_query = "SELECT id, title, author, year, genre FROM books ORDER BY id DESC";
    $result = mysqli_query($connect, $select_query);
    if($result){
        if(mysqli_num_rows($result) > 0){
            $counter = 0;
            while($row = mysqli_fetch_assoc($result)){
                $counter++;
                ?>
                <tr>
                    <td><?=$row['id']?></td>
                    <td><?=$counter?></td>
                    <td><?=$row['title']?></td>
                    <td><?=$row['author']?></td>
                    <td><?=$row['year']?></td>
                    <td><?=$row['genre']?></td>
                    <td><a href="?nav=edit&&id=<?=$row['id']?>">Edit</a></td>
                    <td><a href="?nav=delete&&id=<?=$row['id']?>">Delete</a></td>
                </tr>
                <?php
            }
        }else{
            echo "The table is empty!!";
        }
    }
    ?>
    </tbody>
    <thead>
    <tr>
        <th>Id</th>
        <th>numbers</th>
        <th>Title</th>
        <th>Author</th>
        <th>Year</th>
        <th>Genre</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
    </thead>
    <tfoot>
    
    </tfoot>
</table>
