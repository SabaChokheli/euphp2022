<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
</head>
<body>
    
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
<main>

    <div class="page-header-title">
    <div class="container">
        <div class="page-header-row">
            <a href="../index.php" class="home-icon">
                <img src="https://bookshop.ge/images/icons/home.png" alt="home">
            </a>
            <h4 class="page-title">
                <a href="../index.php">Home</a>
                                                                                &gt; <a href="">Privacy Police</a>
                                                                    </h4>
        </div>
    </div>
</div>

    <div class="police-wrapper">
            <div class="container">
                <div class="police-content">
                    <p class="text mb-40">This privacy policy ('Privacy Policy') explains our approach to any personal information that we might collect from you or which we have obtained about you from a third party and the purposes for which we process your personal information. This Privacy Policy also sets out your rights in respect of our processing of your personal information. This Privacy Policy will inform you of the nature of the personal information about you that is processed by us and how you can request that we delete, update, transfer it and/or provide you with access to it.This Privacy Policy is intended to assist you in making informed decisions when using the Sites and our Services. Please take a moment to read and understand it. Please also note that this Privacy Policy only applies to the use of your personal information obtained by us, it does not apply to your personal information collected during your communications with third parties.</p>
                    <div class="police-item-title">
                        <h3>Who are we and what do we do?</h3>
                        <div class="hr"></div>
                    </div>
                    <div class="text-bg">
                        <p>If you're looking to speak to a member of English Book Education, find out about your online order, or comment on our bookshop, you can get in touch with us by choosing one of the options below.</p>
                    </div>
                    <div class="police-item-title  mb-30">
                        <h3>How to contact us?</h3>
                        <div class="hr"></div>
                    </div>
                    <p class="text">If you have any questions about this Privacy Policy or want to exercise your rights set out in this Privacy Policy, please contact us by:</p>
                    <p class="text-secondary mb-40"><span>Email:</span> englishbookteam@englishbook.ge</p>
                    <div class="police-item-title">
                        <h3>What Personal information do we collect and how do we use it?</h3>
                        <div class="hr"></div>
                    </div>
                    <p class="text mb-30">Our primary goal in collecting personal information from you is to:</p>
                    <ul class="text-list mb-20">
                        <li><p class="text">verify your identity;</p></li>
                        <li><p class="text">help us improve our products and services and develop and market new products and services;</p></li>
                        <li><p class="text">carry out requests made by you on the Sites;</p></li>
                        <li><p class="text">investigate or settle inquiries or disputes;</p></li>
                        <li><p class="text">comply with any applicable law, court order, other judicial process, or the requirements of a regulator;</p></li>
                        <li><p class="text">enforce our agreements with you;</p></li>
                        <li><p class="text">protect the rights, property or safety of us or third parties, including our other customers and users of the Sites and at our shops;</p></li>
                        <li><p class="text">provide support for the provision of our Services; and</p></li>
                        <li><p class="text">use as otherwise required or permitted by law</p></li>
                    </ul>
                    <p class="text mb-40">In particular, we use your personal information for the following purposes:</p>
                    <div class="police-item-title">
                        <h3>Fulfilment of products and services</h3>
                        <div class="hr"></div>
                    </div>
                    <p class="text mb-40">We collect and maintain personal information that you voluntarily submit to us during your use of the Sites and/or our Services, including registering on our Sites. <br>We use the contact and payment details you have provided to us so that we can fulfil the supply of products and services you have ordered from our Sites, or in our shops. This includes your purchase of products, your attendance at events hosted by us or our third party partners and the processing of returns and refunds.</p>
                    <h3 class="head-text">Who do we share your personal information with for this purpose? </h3>
                    <p class="text mb-40">We will share your personal information with the following categories of third parties:</p>
                    <ul class="text-list mb-40">
                        <li><p class="text">To verify card payments and process refunds - credit card companies and other payment providers;</p></li>
                        <li><p class="text">To deliver products and process returns - delivery and courier companies;</p></li>
                        <li><p class="text">Competitions and prize draws - any applicable third party co-promoter (for example, a book publisher), prize supplier, facilitator or publicity  platform.</p></li>
                    </ul>
                    <h3 class="head-text">What is our legal basis?</h3>
                    <p class="text mb-40">It is necessary for us to use your personal information to perform our obligations in accordance with any contract that we may have with you.</p>
                    <div class="police-item-title">
                        <h3>Customer support</h3>
                        <div class="hr"></div>
                    </div>
                    <p class="text mb-20">Our Sites and our shop use various user interfaces to allow you to request information about our products and services: these include printed and electronic enquiry forms and a telephone enquiry service. Your contact information may be requested in each case, together with details of other personal information that is relevant to your customer service enquiry. This information is used in order to enable us to respond to your requests.</p>
                    <h3 class="head-text">Who do we share your personal information with for this purpose?</h3>
                    <p class="text mb-20">We do not share your personal information for this purpose.</p>
                    <h3 class="head-text">What is our legal basis?</h3>
                    <p class="text mb-40">It is in our legitimate interest to use your personal information in such a way to ensure that we provide the very best customer service we can to you.</p>
                    <div class="police-item-title">
                        <h3>Your feedback about our products and services</h3>
                        <div class="hr"></div>
                    </div>
                    <p class="text mb-20">From time to we will contact you to invite you to provide feedback about our products and services in the form of online, postal or in-shop surveys. We use this information to help us improve the quality of service provided by our staff and in our shop. We also use your feedback to monitor the quality of our products and services and to assist us with the selection of future product and service lines.</p>
                    <h3 class="head-text">Who do we share your personal information with for these purposes</h3>
                    <p class="text mb-20">We do not share your personal information for this purpose.</p>
                    <h3 class="head-text">What is our legal basis?</h3>
                    <p class="text mb-40">It is in our legitimate interest to use your personal information in such a way to ensure that we provide the very best customer service we can to you.</p>
                    <div class="police-item-title">
                        <h3>Customer insight and analysis</h3>
                        <div class="hr"></div>
                    </div>
                    <p class="text mb-20">We analyse your contact details and other personal information that we collect about you such as your interests and demographic information together with personal information that we obtain from observing your interactions with our Sites, our email communications to you and/or with our products and services in our shops, such as the products and services you have purchased or viewed, or from your use of the wifi in our shop. Where we have your consent to do so, we will also use your location data collected by us via our mobile application. Where you have given your consent (where lawfully required), we use cookies, log files and other similar technologies to collect personal information from the computer hardware and software you use to access the Sites, or from your mobile. This includes the following:</p>
                    <ul class="text-list mb-20">
                        <li><p class="text">an IP address to monitor Sites traffic and volume;</p></li>
                        <li><p class="text">a session ID to track usage statistics on our Sites;</p></li>
                        <li><p class="text">information regarding your personal or professional interests, demographics, buying habits, experiences with our products and contact preferences.</p></li>
                    </ul>
                    <p class="text mb-20">Our web pages and e-mails contain cookies or pixel tags ("Tags"). Tags allow us to track receipt of an e-mail to you, to count users that have visited a web page, opened an e-mail and to collect other types of aggregate information. Once you click on an e-mail that contains a Tag, your contact information may subsequently be cross-referenced to the source e-mail and the relevant Tag. In some of our e-mail messages, we use a click-through URL linked to certain websites administered by us (or on our behalf) and/or to certain third party websites (including certain event partner websites). If you decide to click through to the relevant URL, we or the relevant third party website operator (as applicable) may through the use of a Cookie track click-through data to assist in determining interest in particular topics and measure the effectiveness of these communications. Please note that to the extent that you click through to a third party website from our email, use of your data by the relevant third party will be subject to their own rules and policies. Please note that any emails you receive from us may contain Cookies to help us to see if recipients have opened an email and understand how recipients have interacted with it. If you have enabled images, Cookies may also be set on your computer or mobile device. If you do not wish to accept Cookies from any one of our emails, simply close the email before downloading any images or refrain from clicking on any URL links. You can also set your browser to restrict Cookies or to reject them entirely. These settings will apply to all Cookies whether included on websites or in emails. In some instances, depending on your email or browser settings, Cookies in an email may be automatically accepted (for example, when you've added an email address to your address book or safe senders list). Please refer to your email browser or device instructions for more information on this. The personal information described in this section is used to create profiles and insights about your shopping habits and the shopping habits of our other customers. By using this information, we are able to measure the effectiveness of our content and how visitors use our Sites and our Services. This allows us to learn what pages of our Sites are most attractive to our visitors, which parts of our Sites are the most interesting and what kind of offers our users like to see. We also use this information to help us with the selection of future product and service lines and the design of our Sites and Services. We also use this information for marketing purposes (see the marketing section below for further details).</p>
                    <h3 class="head-text">Who do we share your personal information with for these purposes</h3>
                    <p class="text mb-20">We do not share your personal information for this purpose.</p>
                    <h3 class="head-text">What is our legal basis?</h3>
                    <p class="text mb-40">It is in our legitimate interest to use your personal information in such a way to ensure that we provide the very best customer service we can to you.</p>
                    <div class="police-item-title">
                        <h3>Marketing communications</h3>
                        <div class="hr"></div>
                    </div>
                    <p class="text mb-20">We carry out the following marketing activities using your personal information:</p>
                    <h3 class="head-text">Email marketing</h3>
                    <p class="text mb-20">We use information that we observe about you from your interactions with our Sites, our email communications to you and/or with our products and services in our shops (see the Customer Insight and Analysis section above for more details of the information collected and how it is collected) to send you marketing communications by email, where you have consented to receive such marketing communications.</p>
                    <h3 class="head-text">Who do we share your personal information with for these purposes?</h3>
                    <p class="text mb-20">Typically, we do not share your personal information for this purpose, but from time to time we may share your personal information with event third parties with your consent.</p>
                    <h3 class="head-text">What is our legal basis?</h3>
                    <p class="text mb-40">Where your personal information is completely anonymised, we do not require a legal basis to use it for marketing analysis as the personal information will no longer constitute personal information that is regulated under applicable data protection laws. Where your personal information is not in an anonymous form, it is in our legitimate interest to use your personal information for personalised marketing purposes.<br>We will only send you marketing communications via email where you have consented to receive such marketing communications.</p>
                    <div class="police-item-title">
                        <h3>Business administration and legal compliance</h3>
                        <div class="hr"></div>
                    </div>
                    <p class="text mb-20">We use your personal information for the following business administration and legal compliance purposes:</p>
                    <ul class="text-list mb-20">
                        <li><p class="text">to comply with our legal obligations;</p></li>
                        <li><p class="text">to enforce our legal rights;</p></li>
                        <li><p class="text">protect rights of third parties; and</p></li>
                        <li><p class="text">in connection with a business transition such as a merger, acquisition by another company, or sale of all or a portion of our assets.</p></li>
                    </ul>
                    <h3 class="head-text">Who do we share your personal information with for these purposes?</h3>
                    <p class="text mb-20">We will share your personal information with professional advisers such as lawyers and accountants and/or governmental or regulatory authorities.</p>
                    <h3 class="head-text">What is our legal basis? </h3>
                    <p class="text mb-20">Where we use your personal information in connection with a business transition, enforce our legal rights, or to protect the rights of third parties it is in our legitimate interest to do so. For all other purposes described in this section, it is our legal obligation to use your personal information to comply with any legal obligations imposed upon us such as a court order. Where we share your sensitive personal information, we shall obtain your consent to do so. Any other purposes for which we wish to use your personal information that are not listed above, or any other changes we propose to make to the existing purposes will be notified to you using your contact details.</p>
                    <div class="police-item-title">
                        <h3>How do we obtain your consent?</h3>
                        <div class="hr"></div>
                    </div>
                    <p class="text mb-20">Where our use of your personal information requires your consent, you can provide such consent:</p>
                    <ul class="text-list mb-20">
                        <li><p class="text">at the time we collect your personal information following the instructions provided; or</p></li>
                        <li><p class="text">by informing us by e-mail, post or telephone using the contact details set out in this Privacy Policy.</p></li>
                    </ul>
                    <h3 class="head-text footer-line">Our use of cookies and similar technologies</h3>
                    <p class="text mb-20">Our Sites use certain cookies, pixels, log files and other technologies of which you should be aware.</p>
                    <h3 class="head-text footer-line">How long do we keep your personal information:</h3>
                    <p class="text mb-20">We will keep your personal information for the purposes set out in this Privacy Policy and in accordance with the law and relevant regulations. We will never retain your personal information for longer than is necessary.</p>
                    <h3 class="head-text footer-line">Confidentiality and Security of your personal data</h3>
                    <p class="text mb-20">We are committed to keeping the personal information you provide to us secure and we will take reasonable precautions to protect your personal information from loss, misuse or alteration. We have implemented information security policies, rules and technical measures to protect the personal information that we have under our control from:</p>
                    <ul class="text-list mb-20">
                        <li><p class="text">unauthorised access;</p></li>
                        <li><p class="text">improper use or disclosure;</p></li>
                        <li><p class="text">unauthorised modification; and</p></li>
                        <li><p class="text">unlawful destruction or accidental loss.</p></li>
                    </ul>
                    <p class="text mb-20">All of our employees and data processors (i.e. those who process your personal information on our behalf, for the purposes listed above), who have access to, and are associated with the processing of personal information, are obliged to respect the confidentiality of the personal information of all users of our Services.</p>
                    <h3 class="head-text footer-line">Changes in Privacy Policy</h3>
                    <p class="text mb-20">We may make changes to this Privacy Policy from time to time. To ensure that you are always aware of how we use your personal information we will update this Privacy Policy from time to time to reflect any changes to our use of your personal information. We may also make changes as required to comply with changes in applicable law or regulatory requirements. We will notify you by e-mail of any significant changes. However, we encourage you to review this Privacy Policy periodically to be informed of how we use your personal information.</p>
                </div>
            </div>
        </div>

</main>
</body>