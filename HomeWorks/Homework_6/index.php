<?php require_once "connect.php"?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>davaleba_6</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="content">
        <h3>დავალება 1</h3>
        <table>
            <tbody>
        <?php
            $select_query = "SELECT `Text`, `Title` FROM menu";
            $result = mysqli_query($connect, $select_query);
            
            if($result){
                
                if(mysqli_num_rows($result) > 0) {
                    $row = mysqli_fetch_assoc($result);
                ?>
                <tr>
                    <td><?=$row['Text']?></td>
                    <td><?=$row['Title']?></td>
                </tr>
                <?php 
                }   
            }
        ?>
            </tbody>
            <thead>
                <tr>
                    <td><b>Text</b></td>
                    <td><b>Title</b></td>
                </tr>
            </thead>
        </table>


        <h3>დავალება 2</h3>
        <table>
            <tbody>
        <?php
            $select_query = "SELECT `Text`, `Title` FROM menu";
            $result = mysqli_query($connect, $select_query);
            
            if($result){
                
                if(mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)){
                ?>
                <tr>
                    <td><?=$row['Text']?></td>
                    <td><?=$row['Title']?></td>
                </tr>
                <?php 
                }   
            }
        }
        ?>
            </tbody>
            <thead>
                <tr>
                    <td><b>Text</b></td>
                    <td><b>Title</b></td>
                </tr>
            </thead>
        </table>

        <h3>დავალება 3</h3>
        <table>
            <tbody>
        <?php
            $select_query = "SELECT * FROM menu WHERE Id = 2";
            $result = mysqli_query($connect, $select_query);
            
            if($result){
                
                if(mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)){
                ?>
                <tr>
                    <td><?=$row['Id']?></td>
                    <td><?=$row['Title']?></td>
                    <td><?=$row['Meta_k']?></td>
                    <td><?=$row['Meta_d']?></td>
                    <td><?=$row['Text']?></td>
                </tr>
                <?php 
                }   
            }
        }
        ?>
            </tbody>
            <thead>
                <tr>
                    <td><b>Id</b></td>
                    <td><b>Title</b></td>
                    <td><b>Meta_k</b></td>
                    <td><b>Meta_d</b></td>
                    <td><b>Text</b></td>
                </tr>
            </thead>
        </table>

        <h3>დავალება 4</h3>
        <table>
            <tbody>
        <?php
            $select_query = "SELECT * FROM menu WHERE Id >=2";
            $result = mysqli_query($connect, $select_query);
            
            if($result){
                
                if(mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)){
                ?>
                <tr>
                    <td><?=$row['Id']?></td>
                    <td><?=$row['Title']?></td>
                    <td><?=$row['Meta_k']?></td>
                    <td><?=$row['Meta_d']?></td>
                    <td><?=$row['Text']?></td>
                </tr>
                <?php 
                }   
            }
        }
        ?>
            </tbody>
            <thead>
                <tr>
                    <td><b>Id</b></td>
                    <td><b>Title</b></td>
                    <td><b>Meta_k</b></td>
                    <td><b>Meta_d</b></td>
                    <td><b>Text</b></td>
                </tr>
            </thead>
        </table>

        <h3>დავალება 5</h3>
        <table>
            <tbody>
        <?php
            $select_query = "SELECT * FROM menu WHERE Id <=4";
            $result = mysqli_query($connect, $select_query);
            
            if($result){
                
                if(mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)){
                ?>
                <tr>
                    <td><?=$row['Id']?></td>
                    <td><?=$row['Title']?></td>
                    <td><?=$row['Meta_k']?></td>
                    <td><?=$row['Meta_d']?></td>
                    <td><?=$row['Text']?></td>
                </tr>
                <?php 
                }   
            }
        }
        ?>
            </tbody>
            <thead>
                <tr>
                    <td><b>Id</b></td>
                    <td><b>Title</b></td>
                    <td><b>Meta_k</b></td>
                    <td><b>Meta_d</b></td>
                    <td><b>Text</b></td>
                </tr>
            </thead>
        </table>