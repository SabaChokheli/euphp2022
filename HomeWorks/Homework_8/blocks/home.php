<h1>ვაკანსიები</h1>
<hr>
<table class="data-table">
    <tbody>
    <?php
    $select_query = "SELECT id, job, company FROM vacancy ORDER BY id DESC";
    $result = mysqli_query($connect, $select_query);
    if($result){
        if(mysqli_num_rows($result) > 0){
            while($row = mysqli_fetch_assoc($result)){
                ?>
                <tr>
                    <td><?=$row['id']?></td>
                    <td><?=$row['job']?></td>
                    <td><?=$row['company']?></td>
                    <td><a href="hire.php">ფორმის გაგძავნა</a></td>
                </tr>
                <?php
            }
        }else{
            echo "The table is empty!!";
        }
    }
    ?>
    </tbody>
    <thead>
    <tr>
        <th>Id</th>
        <th>Job</th>
        <th>Company</th>
        <th></th>
    </tr>
    </thead>
    <tfoot>
    
    </tfoot>
</table>