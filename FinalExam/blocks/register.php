<?php
   $host = "localhost";
   $user = "root";
   $password = "";
   $dbname = "final_exam(vacancy)";

   //$salte = "H34#ksdnj&jd";

   $connect = mysqli_connect($host, $user, $password, $dbname);
   if(!$connect){
      die("Error with mysql connection!!!");
   }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Registration</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="/Project/styles.css">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
</head>
<body>

<nav class="navbar navbar-expand navbar-dark bg-dark" aria-label="Second navbar example">
    <div class="container-fluid">
      <a class="navbar-brand" href="../index.php">Something</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
    </div>
  </nav>


<div class="container mt-5" style="margin: auto;
    width: 50%;
    border: solid 1px;
    padding: 40px;
    height: 60%;">
  <h3>Registration</h3>
  <p>Try to submit the form.</p>
    
  <form method="post" class="was-validated">
  <div class="mb-3 mt-3">
      <label for="uname" class="form-label">Email:</label>
      <input type="text" class="form-control" id="uname" placeholder="Enter Email" name="email" required>
      <div class="valid-feedback">Valid.</div>
      <div class="invalid-feedback">Please fill out this field.</div>
    </div>
    <div class="mb-3 mt-3">
      <label for="uname" class="form-label">Username:</label>
      <input type="text" class="form-control" id="uname" placeholder="Enter username" name="username" required>
      <div class="valid-feedback">Valid.</div>
      <div class="invalid-feedback">Please fill out this field.</div>
    </div>
    <div class="mb-3">
      <label for="pwd" class="form-label">Password:</label>
      <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="password" required>
      <div class="valid-feedback">Valid.</div>
      <div class="invalid-feedback">Please fill out this field.</div>
    </div>
    

    <div class="mb-3">
      <label for="pwd" class="form-label">Re-enter Password:</label>
      <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="re_password" required>
      <div class="valid-feedback">Valid.</div>
      <div class="invalid-feedback">Please fill out this field.</div>
    </div>
    
    <div class="form-check mb-3">
      <input class="form-check-input" type="checkbox" id="myCheck"  name="remember" required>
      <label class="form-check-label" for="myCheck">I agree with terms.</label>
      <div class="valid-feedback">Valid.</div>
      <div class="invalid-feedback">Check this checkbox to continue.</div>
    </div>
  <input type="submit" class="btn btn-primary btn-lg" style="margin-left: 30%; width: 35%;" name="register" value="Register Now">
  </form>
</div>

</body>
</html>

<?php
  if(isset($_POST['register'])){
        $email = $_POST['email'];
        $username = $_POST['username'];
        $password = $_POST['password'];
        $re_password = $_POST['re_password'];

        if($password==$re_password){
            $insert = "INSERT INTO users(email, username, password) 
                    VALUES ('$email', '$username', '$password')";
            if(!mysqli_query($connect, $insert)){
                die("Error with mysql insert query!!!");
            }else{
                header("location: .//index.php");
            }
        }else{
            die("Sign Up Error!!!");
        }
  }
?>