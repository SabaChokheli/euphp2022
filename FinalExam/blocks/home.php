
<br><br>
<div class="container mt-3">
  <h2>Vacancy</h2> 
  <table class="table">
    <thead style="padding: 30px;">
      <tr>
        <th>ID</th>
        <th>Job Description</th>
        <th>Company</th>
        <th>Date</th>
      </tr>
    </thead>
    <tbody>
    <?php
            $select_query ="SELECT * FROM content ORDER BY id ASC";
            $result = mysqli_query($connect, $select_query);
            if($result){
                if(mysqli_num_rows($result) > 0){
                    while($row = mysqli_fetch_assoc($result)){
                    ?>
                    <tr>
                        <td><?=$row['id']?></td>
                        <td><?=$row['description']?></td>
                        <td><?=$row['company']?></td>
                        <td><?=$row['date']?></td>
                        
                    </tr>
                    <?php 
                }
            }else {
                echo "The table is empty";
            }
        }
        ?>
    </tbody>
  </table>
</div>