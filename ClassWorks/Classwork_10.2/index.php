<?php
    include_once "mysql/connect.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <?php 
        include_once "blocks/header.php"
    ?>
   <div class="body_content">
        <div class="links">
            <ul>
                <li><a href="">HTML</a> </li>
                <li><a href="">CSS</a> </li>
                <li><a href="">PHP</a> </li>
            </ul>
        </div>
        <div class="blog_post">
            <h2>FullStack - How to create a working blogging website with pure HTML, CSS and JS in 2021.</h2>
            <p>Hello, Today we'll see, how we can easily create a blogging website using HTML, CSS and JS only. No other library. We'll also use Firebase firestore to store/retrieve blog data.

This is a very good project to practice full-stack development. When I started with web development I always thought how can I make my own blogging website. And today, I am proud that I tried to make blogging site. Our website is very simple and has features like</p><br>
<h4>Folder Structure</h4>
<p>This is our folder structure.</p>
<img src="https://res.cloudinary.com/practicaldev/image/fetch/s--g3gQlMsG--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/uploads/articles/k105l49v9om0hhwbphvl.png">

        </div>
   </div>
       <?php 
        include_once "blocks/footer.php"
       ?>
</body>
</html>